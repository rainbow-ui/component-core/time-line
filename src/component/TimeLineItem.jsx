import {Component} from "rainbowui-desktop-core";
import {Util} from "rainbow-desktop-tools";
import config from "config";
import PropTypes from 'prop-types';

export default class TimeLineItem extends Component {

	renderComponent() {
		return (
			<li className="timeline-item">
				<div className={"timeline-badge " + this.props.styleClass}><i className={this.props.icon} /></div>
				<div className="timeline-panel">
					<div className="timeline-heading">
						{
							this.props.hasIM && this.props.title.length > 0 ?
							<h4 className="timeline-title curse" onClick={this.props.ChartWithIM.bind(this)}>{this.getI18n(this.props.title)}
							<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAAEEfUpiAAAABGdBTUEAALGPC/xhBQAABHZJREFUWAntV1toXEUY/ubsbrpbL0WMqdqk2gd9qFShguKDdpPUS7ygqCC1Nd0txRdRQaF4QcyDICqKxDeV7CY21npDkBq0aTfqk603NOpDX6wJmiwBL5Qk3e2e8fsnO5Nzzp4YUyModODszH+/zD//zAJxo72oOwWf7dNjqr7QltEzC4UXR3YoQ7SE+blrtz5zHgK81Blw8lTq1kGm+XW2qF9qL+hj7X361nksV0R+EULUAVFp3KLmjXEMxE0tgF8C2kUr8c1U8TVlJ0fy6qqQjrj4sgV9yDAxgttD3BHAYwRFSbvgo7PhveV9vTIiZMCOot7i8OJDZ7++1CJotnjToD7Lwv/u7PIkZnq4NSMFzEIhye+JtMZgqhnl6Slc7gNPMeBrKPBYaYd6usEtiYPJbm0gxCDIW+a3XUimGJj0bxIJXHa8hg0kmJQKsaNfd8XBrOcWkovCYwbrb29HQd9v4b8zU3G4uunFEJHfLiZ87R59flA4aQUYy2dM1A08Fc8pjW7qFzdDgwns3b9FPUhkKPkhpv8fEBuLdAWt8AiJG5jqMiPel0ni8aFt6o9oiHO9qo6l4KMmwwoPJYBdpzejJc1yIHmCp/k30ipSrUElzgNzgjXOY5G0BRmCa2mb2sdwpgmrrDdGG4soxwZ44V8Ji6JSTh2gcNtMBb8HFUsnCFdViNoISKO0rSYUDxWNSf1bkYVgz0OeFnuEL6SAcKtfw8tCqI9YWGVwlNW61jItOQQ5eAzhTVEw54HCBHvanU7jIgtfoze9Gvc4Bdkc1tRqeItKzl5EVrw9wtvto6Eb1XHhdXXAIvqO/uzldq4u5dV9UUWdu/X6WhWjFPiU9E2W7o4zs7pe+dguZRu3rX4F094KrDu4VR21wqfm/0YGXBUs5M7mAb22dgIPk76TGx17UUdlqXSauFcTSTw/3K1+itKDcKwDcrFPV/Ce3KSsKvZGDHgpPHtgm/o+KLzQWkrWr2IXHe5mWSvW/Scrm3Dbvq3q16hMgwM8oh/S8HU0fCSVRpa30M9RoaXAcg1WZzFCRy6SA8gzdH1Q3jnQ9YFeMTOBcRKb2a3uPphTe4KM/3Qt7ynfx+vUM5U5F622E7huODuJ14xxhQeW27g4Lzo96hYbdVuC5ivKDkk7h3caBmSWXs7JPpbGo7fNydBFt38MvdwOY0vsuAyw0n4RhK5gjSEkcC9n2ZJxb24taDfquCXR9QwuEAW05U6GqwFGdBdpb7BQDrFQrnSWlnEhVykLfCNt3EEb74pq54AAHX16J99nr5ChrJpwxXI1bnn38iHwJU2cwy/H7ewXezJCDghC/pHMVnGYnl5MR16gp9KETmrUnzFFCrfS0uFMCpvtc8YqbHCAQpfw7TNqGSLzKCt5kM59xeoZTycxllyFE9UptFSBtpriXwAWGBvQ1dKAKFvlz5Ob8nimR/GyjBmNDhT02yySH1Ia7+zPK/lvZkZPSSc//pHVq3Ez6esoKA84+RI0WOY8SZOf08xwugmlaKRGyamfmAz8CasD2JnwW7DXAAAAAElFTkSuQmCC" />
							</h4>
							:
							<h4 className="timeline-title" >{this.getI18n(this.props.title)}
							</h4>
						}
						{
							this.props.icon && this.props.icon !== '' ?
							<span onClick={this.props.onClickTitleIcon} className={"timeline-title-icon " + this.props.titleIcon}></span>
							:''
						}
						<p className="timeline-title-description">{this.getI18n(this.props.titleDescription)}</p>
					</div>
					<div className="timeline-body">
						<p>{this.getContent()}</p>
					</div>
					<div className="timeline-time">
						<p onClick={this.props.onClickTimeIcon}><small className="text-muted"><i className="glyphicon glyphicon-time" />{this.getDateTime()}</small></p>
					</div>
				</div>
			</li>
		);
	}


	getContent(){
		let {content} = this.props;
		if(content && Util.isArray(content)){
			let contentArray = [];
			$.each(content, (index, element) => {
				contentArray.push(<div>{element}</div>);
			});
			return contentArray;
		}
		return this.props.content;
	}

	getDateTime(){
		let {dateTime, format} = this.props;
		if(dateTime != undefined){
			return dayjs(dateTime).format(format);
		}
	}

};


/**
 * TimeLineItem component prop types
 */
TimeLineItem.propTypes = $.extend({}, Component.propTypes, {
	icon: PropTypes.string,
	title: PropTypes.string,
	content: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
	dateTime: PropTypes.string,
	hasIM: PropTypes.bool,
	onClickTimeIcon: PropTypes.func,
	onClickTitleIcon: PropTypes.func,
	//format: PropTypes.string,
	styleClass: PropTypes.oneOf(["default", "primary", "success", "warning", "danger", "info"])
});

/**
 * Get timeLineItem component default props
 */
TimeLineItem.defaultProps = $.extend({}, Component.defaultProps, {
	styleClass: config.DEFAULT_STYLE_CLASS,
	hasIM: false,
	icon: "glyphicon glyphicon-time"
});